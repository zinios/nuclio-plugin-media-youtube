<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\media\youtube
{
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	use nuclio\exception\NuclioException;
	use \Google_Client as GoogleClient;
	use \Google_Service_YouTube as GoogleYoutube;
	
	class Youtube extends Plugin
	{

		private ?string $Oauth2_clientId;

		private ?string $Oauth2_clientSecret;

		private string $developerKey = '';

		private string $scopeUrl = 'https://www.googleapis.com/auth/youtube';

		private ?string $proxy;

		private ?GoogleClient $client;

		private ?GoogleYoutube $gtube;

		private string $APIKey = 'AIzaSyBh4EsKKuR8P-sGPC2coml01NO01JZG_p8';

		public static function getInstance(...$args):Youtube
		{
				$instance=ClassManager::getClassInstance(self::class);
				return ($instance instanceof self)?$instance:new self(...$args);
		}

		public function __construct(Map $config=null)
		{
	 		parent::__construct();
	 		if(!is_null($config))
	 		{
	 			if($config->containsKey('proxy') && !is_null($proxy = $config->get('key')))
	 			{
	 				$this->proxy = $proxy;
	 			}
	 		}
		}

		public function Search(string $keyword, int $max=20):array<string,mixed>
		{
			$this->gtube=new GoogleYoutube($this->client);
			try 
			{
				$searchResponse = $this->gtube->search->listSearch('id,snippet', array(
						'q' 			=> $keyword,
						'maxResults'	=> $max
					));
				return $searchResponse;
			}
			catch(Exception $e)
			{
				throw new NuclioException($e->getMessage());
			}
		}

		public function curl_get(string $url):mixed
		{
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_TIMEOUT, 30);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
			$return = curl_exec($curl);
			curl_close($curl);
			return $return;
		}


		public function GrabVideoDetails(string $url):mixed
		{
			$matches = [];
			$regExp = '/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/';
			preg_match($regExp, $url, $matches);
			$id = end($matches);
			$apiEndpoint  = 'https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails&id='.$id.'&key='.$this->APIKey;
			if (!is_null($this->proxy))
			{
				$context=stream_context_create
				(
					[
						'http'=>
						[
							'proxy'				=>$this->proxy,
							'request_fulluri'	=>true
						]
					]
				);
				$videoDetails	=file_get_contents($apiEndpoint, false, $context)
								|>json_decode($$);
			}
			else
			{
				$videoDetails=json_decode(file_get_contents($apiEndpoint));
			}
			return $videoDetails;
		}


	}
}
